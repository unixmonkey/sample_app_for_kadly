class SessionsController < ApplicationController

  def new
  end

  def create
    user = User.find_by(phone: params[:session][:phone].downcase)
    if user && user.authenticate(params[:session][:password])
      sign_in user
      redirect_back_or root_url#user
    else
      flash.now[:error] = 'Неверная комбинация телефон/пароль'
      render 'new'
    end
  end

  def destroy
    sign_out
    redirect_to root_url
  end
	
end
