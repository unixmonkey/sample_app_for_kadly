class StaticPagesController < ApplicationController
  def home
  end

  def admin
  end

  def passengers
  end

  def profile
  end

  def new
  end

  def enter
  end

  def help
  end

end
