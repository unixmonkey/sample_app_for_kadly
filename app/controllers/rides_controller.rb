class RidesController < ApplicationController
  def new
  end

  def for
    render partial: valid_autos.detect{|p| p == params[:auto] }
  end

  private

  def valid_autos
    ['gazel', 'hundai', 'mercedes']
  end
end
