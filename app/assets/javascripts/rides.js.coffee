# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
  $('#transport_gazel').click ->
    $('#partial-area').load('/rides/for/gazel')

  $('#transport_mercedes').click ->
    $('#partial-area').load('/rides/for/mercedes')

  $('#transport_hyundai').click ->
    $('#partial-area').load('/rides/for/hundai')
