namespace :db do
  desc "Fill database with sample data"
  task populate: :environment do
    User.create!(name: "Example User",
                 lastname: "Example User",
                 phone: "123456",
                 settlement: "Settlement",
                 password: "foobar",
                 password_confirmation: "foobar")
    99.times do |n|
      name  = Faker::Name.name
      phone = "1-#{n+1}"
      settlement = "Травное"
      password  = "password"
      User.create!(name: name,
                   lastname: lastname,
                   phone: phone,
                   password: password,
                   password_confirmation: password)
    end
  end
end